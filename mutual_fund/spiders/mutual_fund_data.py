# -*- coding: utf-8 -*-
import scrapy
import urllib.parse as urlparse
import re
import ast
from bs4 import BeautifulSoup
import unicodedata


class MutualFundDataSpider(scrapy.Spider):
    name = 'mutual_fund_data'
    #allowed_domains = ['http://www.fipiran.com']
    start_urls = ['http://www.fipiran.com/Fund/MFAll/']
    base_url = "http://www.fipiran.com"
    base_get_url = base_url + '/Fund/MFwithRegNo?regno='
        
    def parse(self, response):
        for row in response.xpath('//*[@class="table table-striped tablesorter"]//tr'):
            mutual_fund_url = row.xpath('td[2]//a/@href').extract_first()
            if mutual_fund_url is not None:
                mutual_fund_url = self.base_url + mutual_fund_url
                parsed = urlparse.urlparse(mutual_fund_url)
                fund_number = urlparse.parse_qs(parsed.query)['regno'][0]
                yield scrapy.Request(mutual_fund_url, callback=self.parse_mutual_fund, meta={"item":fund_number})
#                break

    def parse_mutual_fund(self, response):
        info_series = re.findall('data:(.*?) }\]',response.text)
        NAV_series = self.convert_str_to_list(info_series[0])
        UNIT_series = self.convert_str_to_list(info_series[1])
        NAV_Unit = list(zip(NAV_series,UNIT_series))
        info = {nav_unit[0]['x']//1000:{'NAV': nav_unit[0]['y'], 'UNIT':nav_unit[1]['y']} for nav_unit in NAV_Unit}
        fund_number = response.meta['item']
        yield scrapy.Request(self.base_get_url+str(fund_number), callback=self.parse_mutual_fund_specification, meta={"fund_number" :fund_number, "nav_unit": info})

    def parse_mutual_fund_specification(self, response):
        soup = BeautifulSoup(response.text, 'html.parser')
        fund_performance = self.parse_table(soup)
        divs = soup.findAll("div", {"class": "col-md-4 col-xs-12 col-sm-4 col-ms-12"})
        fund_info = {div.contents[0].replace('\u200c', '').replace(" : ",""):unicodedata.normalize("NFKD", div.contents[1].text).strip() for div in divs}
        yield{"fund_number":response.meta["fund_number"], **fund_info, **fund_performance, "nav_unit":response.meta["nav_unit"]}


    def convert_str_to_list(self,txt):
        txt += '}]'
        m = txt.replace(" ","").replace('x','"x"').replace('y','"y"')
        return ast.literal_eval(m)
        
    def parse_table(self,soup):
        table = soup.findAll("table", {"id": "fundDetails"})[1]
        rows = table.find_all("tr")
        table_contents = []   #store your table here
        for tr in rows:
            if rows.index(tr) == 0 : 
                row_cells = [th.getText().strip() for th in tr.find_all('th') if th.getText().strip() != '']  
            else : 
                row_cells = ([tr.find('th').getText() ] if tr.find('th') else []) + [td.getText().strip() for td in tr.find_all('td') if td.getText().strip() != ''] 
            table_contents += [row_cells]
        return {table_contents[0][i]:table_contents[1][i]  for i in range(len(table_contents[0]))}
        