# Scraping Data of Mutual Funds

**Scraping Data of Mutual Funds** is the first part of "Finding the best mutual fund to invest your money" project. 
In this project we gather the required data of different mutual funds by scraping data using the Scrapy library in Python. 

You can find more information about this project in the following link:
[Finding the best mutual fund to invest your money - Part 1: Web scraping with Scrapy in Python](https://medium.com/p/f804de998281/edit)


